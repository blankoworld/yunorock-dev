# yunorock-dev

In order to have an entire YunoRock development environment, I created a 
script that install an Alpine chroot, configure and enter it.

# Usage

Launch it, and start testing!

```bash
chmod +x ./install-and-chroot.sh
./install-and-chroot.sh
```

By default it writes files into `/alpine`. So enter into chroot:

```bash
/alpine/enter-chroot
```

But you can change destination directory if needed:

```bash
chmod +x ./install-and-chroot.sh
DIRPATH="/my_alpine_chroot" ./install-and-chroot.sh
```

and then:

```bash
/my_alpine_chroot/enter-chroot
```

Pay attention: DIRPATH should be an absolute path.

# TODO

  * adapt script to install yunoconfig dependencies (alpine-chroot-install -p)
  * git submodule with yunoconfig (and bind it into the chroot?)
  * idem with yunorock-recipes
  * idem with pkgxx
