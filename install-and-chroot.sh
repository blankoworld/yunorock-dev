#!/usr/bin/env sh

SCRIPT=${SCRIPTNAME:-'alpine-chroot-install'}
DIR=${DIRPATH:-'/alpine'}
script_url="https://raw.githubusercontent.com/alpinelinux/alpine-chroot-install"
script_version="v0.9.0"
script_name="alpine-chroot-install"
FORCE_EXEC=${FORCE_EXEC:-0}

# Tests
if ! [[ $DIR == /* ]]; then
  echo "DIRPATH is not an absolute URL. It should start with a '/'" && exit 1
fi

# DL alpine install script
if ! test -f "$SCRIPT"; then
  wget "${script_url}/${script_version}/${script_name}" -O "$SCRIPT" \
    && echo "e5dfbbdc0c4b3363b99334510976c86bfa6cb251  ${SCRIPT}" | sha1sum -c \
      || exit 1
  chmod +x "$SCRIPT"
fi

# Install Alpine
if ! test -d "$DIR" || ! [[ "$FORCE_EXEC" == 0 ]]; then
  sudo bash ./$SCRIPT -d $DIR -m http://ftp.halifax.rwth-aachen.de/alpine \
    -r /srv/pkgxx/dev \
    -p abuild \
    -p bash \
    -p build-base \
    -p git \
    -p lua-argparse \
    -p lua-filesystem \
    -p lua-toml \
    -p luarocks5.1 \
    -p moonscript \
    -p zsh || exit 1
fi

# Set rc_sys="prefix" to not disturb systemd on current system
sudo sed -e 's/#rc_sys=""/rc_sys="prefix"/g' -i "$DIR/etc/rc.conf"

# Launch chroot
sudo cp build $DIR/build && sh $DIR/enter-chroot /build || exit 1

# Insert pkg++ configuration in chroot
sudo mkdir -p "$DIR/srv/pkgxx/dev" && \
  sudo mkdir -p "$DIR/srv/pkgxx/src" && \
  sudo cp "pkgxx.conf.example" "$DIR/etc/pkgxx.conf" || exit 1

exit 0
